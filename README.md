# Ansible role: siptrack-backend

Role to setup the [siptrackd](https://github.com/sii/siptrackd) backend API for [Siptrack](https://github.com/sii/siptrackweb).

# Dependencies

* Ready mysqld server.
* siptrack user and database created with proper grants and access in mysql server.
* siptrack system user for installing files and running services
* virtualenv directory owned by the siptrack user
* sudoers config allowing your ansible admin user to sudo as the siptrack user

See below for parameters that describe how to specify these dependencies as input to the role.

# Role parameters

## Important parameters

* ``siptrack_username`` - User that will own all siptrack files, including virtualenv, and run the service.
* ``siptrack_home`` - Home dir of the user.
* ``siptrack_db_host`` - DB server host
* ``siptrack_db_user`` - DB server login user
* ``siptrack_db_pass`` - DB server login password
* ``siptrack_db_name`` - DB name
* ``siptrackd_virtualenv`` - Path to virtualenv owned by siptrack user.
* ``siptrackd_host`` - Name or IP that siptrackd service listens to (port 9242 and 9243).

## Other parameters

Check the ''defaults/main.yml'' file for quick access to all the variables you can override.

# Example playbook

**Disclaimer:** These examples include roles I have locally, but they should be self-explanatory.

## Bootstrap

```yaml
---

- hosts: siptrack-backend:siptrack-frontend
  become: True                                                                                           

  vars:
    siptrack_yum:
      - openssl-devel
      - gcc
      - libffi-devel
      - openldap-devel
      - mariadb-devel
      - python-virtualenv
      - git

  roles:
    - role: mariadb_client
      mariadb_version: 5.5
      mariadb_distro: centos7

    - role: sudo_as
      sudoas_user1: stemid
      sudoas_user2: "{{siptrack_username}}"
      sudoas_nopasswd: yes

  tasks:
    - name: install siptrack dependencies on RedHat
      package:
        name: "{{item}}"
        state: installed
      when: ansible_os_family == 'RedHat'
      with_items: "{{siptrack_yum}}"

    - name: create siptrack user
      user:
        name: "{{siptrack_username}}"
        home: "{{siptrack_home}}"
        system: yes
        createhome: yes
```

## Database

```yaml
---

- hosts: siptrack-db
  become: True

  vars_files:
    - vars/db.yml

  roles:
    - role: mariadb
      mariadb_version: 5.5
      mariadb_distro: centos7
      listen_tcp: yes
      bind_address: "{{ansible_default_ipv4.address}}"
  
  tasks:
    - name: create siptrack db
      mysql_db:
        name: "{{siptrack_db.database}}"
        encoding: utf8
        login_user: root
        state: present

    - name: create siptrack db user
      mysql_user:
        name: "{{siptrack_db.username}}"
        password: "{{siptrack_db.password}}"
        priv: "{{siptrack_db.database}}.*:ALL"
        host: app01.mylan.local
        login_user: root
        state: present
```

## Install

```yaml
---

- hosts: siptrack-backend
  become: True

  vars_files:
    - vars/common.yml
    - vars/db.yml

  roles:
    - role: global_virtualenv
      global_virtualenv_path: "{{siptrack_virtualenv}}"
      global_virtualenv_owner: "{{siptrack_username}}"
    
    - role: siptrack-backend
      siptrackd_virtualenv: "{{siptrack_virtualenv}}"
      siptrackd_db_host: "{{siptrack_db.hostname}}"
      siptrackd_db_user: "{{siptrack_db.username}}"
      siptrackd_db_pass: "{{siptrack_db.password}}"
      siptrackd_db_name: "{{siptrack_db.database}}"
```

# Distro support

For now only CentOS/RHEL but Debian/Ubuntu shouldn't be hard to do.
