---

- name: siptrack-backend setup
  block:

  - name: install required OS packages for RedHat
    yum:
      name: "{{ RedHat_packages }}"
      state: present
      enablerepo: epel
    when: ansible_os_family == 'RedHat'

  - name: install required OS packages for Debian
    apt:
      name: "{{ Debian_packages }}"
      state: present
    when: ansible_os_family == 'Debian'

  - name: ensure siptrack_base dir exists
    file:
      path: "{{ siptrack_base }}"
      state: directory
    become_user: "{{ siptrack_username }}"
    become: yes

  - name: create siptrack tmp dir for virtualenv
    file:
      path: "{{ siptrack_virtualenv }}/tmp"
      state: directory
    become_user: "{{ siptrack_username }}"
    become: yes

  - name: install python dependencies in virtualenv
    pip:
      virtualenv: "{{ siptrackd_virtualenv }}"
      name: "{{ siptrackd_pypi_dependencies }}"
    environment:
      - TMPDIR: "{{ siptrack_virtualenv }}/tmp"
    become_user: "{{ siptrack_username }}"
    become: yes

  when: run_setup|bool
  tags: setup

- name: siptrack-backend configuration
  block:

  - name: install siptrackd_storage config
    template:
      src: etc/siptrackd_storage.cfg.j2
      dest: "{{ siptrack_base }}/siptrackd_storage.cfg"
      owner: "{{ siptrack_username }}"
      mode: 0640
    notify: restart siptrackd

  when: run_configure|bool
  tags: configure

- name: siptrack-backend install source code
  block:

  - name: checkout siptrackd git repository
    git:
      repo: "{{ siptrack_repos.siptrackd.url }}"
      dest: "{{ siptrack_base }}/siptrackd"
      version: "{{ siptrack_repos.siptrackd.branch }}"
      accept_hostkey: yes
    register: result
    notify:
      - restart siptrackd
    become_user: "{{ siptrack_username }}"
    become: yes

  - name: install siptrackdlib
    command: "{{ siptrackd_virtualenv }}/bin/python setup.py install"
    args:
      chdir: "{{ siptrack_base }}/siptrackd"
    become_user: "{{ siptrack_username }}"
    become: yes

  - name: install siptrackd service
    template:
      src: "etc/systemd/system/siptrackd.service.j2"
      dest: "/etc/systemd/system/{{ siptrackd_servicename }}.service"
    when: ansible_service_mgr == 'systemd'
    register: systemd_result
    notify:
      - daemon-reload
      - restart siptrackd

  when: run_install|bool
  tags: install

- name: siptrack-backend post install
  block:

  - name: ensure siptrackd is started with systemd
    systemd:
      name: "{{ siptrackd_servicename }}"
      state: started
      enabled: yes
      daemon_reload: yes
    when: ansible_service_mgr == 'systemd'

  when: run_post_install|bool
  tags: post-install
